export interface Duty {
    id: string;
    name: string;
    created_at: string;
    updated_at: string;
    deleted_at: string;
    completed: boolean;
}
export interface EditDuty {
    id: string;
    name: string;
    created_at: string;
    updated_at: string;
    deleted_at: string;
    completed: boolean;
    editing: boolean;
}