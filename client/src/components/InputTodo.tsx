import React from "react";
import { Duty } from "../interfaces/todo";
function InputTodo() {
  const [name, setName] = React.useState("");
  const [id, setId] = React.useState("");

  const handleSubmit = (event: any) => {
    event.preventDefault();
    alert(`The name you entered was: ${name}`);
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ name:  name}),
    };
    fetch("/api/v1/todos", requestOptions)
      .then(response => response.json())
      .then(data => {
        if (data.rowCount) {
            
        }
      });
  };

  return (
    <div className="todo-input">
      <form onSubmit={handleSubmit}>
        <label>
          Name:
          <input
            type="text"
            name="name"
            onChange={e => setName(e.target.value)}
          />
        </label>
        <input type="submit" />
      </form>
    </div>
  );
}

export default InputTodo;
