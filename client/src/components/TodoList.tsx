import React from "react";
import { EditDuty } from "../interfaces/todo";
import { List, Typography, Checkbox, Button, Input, Space } from "antd";
import { SmileOutlined } from "@ant-design/icons";

function TodoList() {
  const [data, setData] = React.useState<EditDuty[]>([]);
  const [name, setName] = React.useState("");
  const { Text } = Typography;
  console.log(`data:`, data);
  React.useEffect(() => {
    fetch("/api/v1/todos")
      .then(res => res.json())
      .then(res => {
        const curData = res.rows.map((row: any) => {
          let el = { ...row };
          el.editing = false;
          return el;
        });
        setData(curData);
      });
  }, []);
  const validName = (val: string) => {
    if (val === "") {
      return false;
    }
    return true;
  };
  const handleSubmit = (event: any) => {
    if (!validName(name)) return;
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ name: name }),
    };
    fetch("/api/v1/todos", requestOptions)
      .then(response => response.json())
      .then(res => {
        if (res.rowCount) {
          const updatedList = [...data, res.rows[0]];
          setData(updatedList);
        }
      });
    setName('');
  };
  const handleSaveEnter = () => {
    if (!validName(name)) return;
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ name: name }),
    };
    fetch("/api/v1/todos", requestOptions)
      .then(response => response.json())
      .then(res => {
        if (res.rowCount) {
          const updatedList = [...data, res.rows[0]];
          setData(updatedList);
        }
      });
    setName('');
  };
  const handleDelete = (id: string) => {
    const requestOptions = {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
    };
    fetch("/api/v1/todos/" + id, requestOptions)
      .then(response => response.json())
      .then(res => {
        if (res.rowCount) {
          const updatedList = data.filter(d => d.id !== res.rows[0].id);
          setData(updatedList);
        }
      });
  };
  const handleDoubleClick = (id: string) => {
    const updatedList = data.map(d => {
      if (d.id === id) {
        d.editing = true;
      }
      return d;
    });
    console.log(`updated list`, updatedList);
    setData(updatedList);
  };
  const handleChange = (event: any, id: string) => {
    const updatedList = data.map(d => {
      if (d.id === id) {
        d.name = event.target.value;
      }
      return d;
    });
    console.log(`updated list`, updatedList);
    setData(updatedList);
  };
  const handleCheckbox = (e: any, id: string) => {
    const updatingItem = data.find(d => d.id === id);
    const requestOptions = {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        name: updatingItem?.name,
        completed: e.target.checked,
        updated_at: new Date().toISOString(),
      }),
    };
    fetch("/api/v1/todos/" + id, requestOptions)
      .then(response => response.json())
      .then(res => {
        if (res.rowCount) {
          const updatedList = data.map(el => {
            return el.id === id ? res.rows[0] : el;
          });
          setData(updatedList);
        }
      });
  };
  const handleBlur = (e: any, id: string) => {
    const updatingItem = data.find(d => d.id === id);
    const requestOptions = {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        name: updatingItem?.name,
        updated_at: new Date().toISOString(),
      }),
    };
    fetch("/api/v1/todos/" + id, requestOptions)
      .then(response => response.json())
      .then(res => {
        if (res.rowCount) {
          const updatedList = data.map(el => {
            return el.id === id ? res.rows[0] : el;
          });
          setData(updatedList);
        }
      });
  };
  const handleEnter = (e: any, id: string) => {
    if (e.key === "Enter") {
      const updatingItem = data.find(d => d.id === id);
      const requestOptions = {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          name: updatingItem?.name,
          updated_at: new Date().toISOString(),
        }),
      };
      fetch("/api/v1/todos/" + id, requestOptions)
        .then(response => response.json())
        .then(res => {
          if (res.rowCount) {
            const updatedList = data.map(el => {
              return el.id === id ? res.rows[0] : el;
            });
            setData(updatedList);
          }
        });
    }
  };
  return (
    <div className="todo-list">
      <h1>Todo List</h1>
      <div className="todo-input">
        <Space.Compact
          style={{
            width: "100%",
          }}
        >
          <Input
            name="name"
            onChange={e => setName(e.target.value)}
            placeholder="Input your todo..."
            onPressEnter={handleSaveEnter}
          />
          <Button type="primary" onClick={handleSubmit}>
            Save
          </Button>
        </Space.Compact>
      </div>
      {!!data.length && (
        <List
          className="Todo-List"
          header={
            <div>
              {" "}
              <SmileOutlined /> My Todo List ({data.length})
            </div>
          }
          footer={<div></div>}
          bordered
          dataSource={data}
          renderItem={item => (
            <List.Item>
              <div
                className="ListItem"
                key={item.id}
                onDoubleClick={e => handleDoubleClick(item.id)}
              >
                {!item.editing ? (
                  <span>
                    <Checkbox
                      className="Complete-Checkbox"
                      checked={item.completed}
                      onChange={e => handleCheckbox(e, item.id)}
                    />
                    {item.completed ? (
                      <Text delete>{item.name}</Text>
                    ) : (
                      <Text>{item.name}</Text>
                    )}
                    <Button
                      className="Delete-Button"
                      type="dashed"
                      onClick={e => handleDelete(item.id)}
                    >
                      Delete
                    </Button>
                  </span>
                ) : (
                  <div>
                    <Input
                      className="Edit-Name"
                      name={item.name}
                      value={item.name}
                      onChange={e => handleChange(e, item.id)}
                      onBlur={e => handleBlur(e, item.id)}
                      onKeyDown={e => handleEnter(e, item.id)}
                    />
                  </div>
                )}
              </div>
            </List.Item>
          )}
        />
      )}
    </div>
  );
}
export default TodoList;
