import React from "react";
import "./App.css";
import TodoList from './components/TodoList';
function App() {
  return (
    <div className="App">
      {/* <p className="title">My Todo List</p> */}
      <TodoList />
    </div>
  );
}

export default App;
