import express, { Request, Response } from 'express';
import dotenv from 'dotenv';
import { router }  from './routers/todo.js';
import bp from 'body-parser';
import path from 'path';
import { fileURLToPath } from 'url';
const __filename = fileURLToPath(import.meta.url);

const __dirname = path.dirname(__filename);

dotenv.config();

const app = express();
const port = process.env.PORT || 3001;

app.use(bp.json());
app.use(bp.urlencoded({ extended: true }))
// serve frontend
app.use(express.static(path.join(__dirname, '../../client/build')));

// serve api
app.use('/api/v1/todos', router);

app.listen(port, () => {
    console.log(`[server]: Server is running at http://localhost:${port}`);
});