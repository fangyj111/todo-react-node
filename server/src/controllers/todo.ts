import { todoServices } from '../services/todo.js';
import { Request, Response } from 'express'
import { v4 as uuidv4 } from 'uuid';


class todoController {
    //get all todo items
    getTodoList = async (req: Request, res: Response) => {
        try {
            const result = await todoServices.getTodoList()
            return res.json(result)
        } catch (err) {
            console.log(`get to do list error`, err);
        }
    }
    getTodo = async (req: Request, res: Response) => {
        try {
            const id = req.params.id;
            const result = await todoServices.getTodo(id)
            return res.send(result?.rows)
        } catch (err) {
            console.log(`get to do error`, err);
        }
    }
    createTodo = async (req: Request, res: Response) => {
        try {
            // TODO: validate todo data
            const data = {
                id: uuidv4(),
                name: req.body.name,
                created_at: new Date().toISOString(),
                completed: false
            };
            console.log(`request body`, JSON.stringify(req.body));
            const result = await todoServices.createTodo(data)
            res.status(201).json(result)
        } catch (err) {
            console.log(`create to do error`, err);
        }
    }
    updateTodo = async (req: Request, res: Response) => {
        try {
            console.log(`request body`, JSON.stringify(req.body));
            const result = await todoServices.getTodo(req.params.id);
            if (!result?.rowCount) {
                console.log(`record doesn't exist!`);
                res.status(404).send('Sorry, can\'t find that record!')
                return
            }
            // TODO: validate todo data
            const todo = await todoServices.updateTodo(req.params.id, req.body)
            res.status(201).json(todo)

        } catch (err) {
            console.log(`update to do error`, err);
        }
    }
    deleteTodo = async (req: Request, res: Response) => {
        try {
            console.log(`request todo id`, JSON.stringify(req.params.id));
            const result = await todoServices.getTodo(req.params.id);
            if (!result?.rowCount) {
                console.log(`record doesn't exist!`);
                res.status(404).send('Sorry, can\'t find that record!')
                return
            }
            const data = {
                updated_at: new Date().toISOString(),
                deleted_at: new Date().toISOString()
            }
            const todo = await todoServices.deleteTodo(req.params.id, data)
            res.status(201).json(todo)


        } catch (err) {
            console.log(`delete to do error`, err);
        }
    }

}
export const TodoController = new todoController()