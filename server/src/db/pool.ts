import dotenv from 'dotenv';
import { parse } from 'path';
import pkg from 'pg';
dotenv.config();

const { Client, Pool } = pkg;

// const db_config = {
//     user: process.env.pg_username,
//     host: process.env.pg_host,
//     port: 55000,
//     database: process.env.db_name,
//     password: process.env.db_password,
// }
// const client = new Client(db_config);
// client.connect().then(() => { console.log('Connected to PostgreSQL database!'); })
// .catch((err) => { console.error('Error connecting to the database:', err); });

export const pool = new Pool({
    user: process.env.pg_username,
    host: process.env.pg_host,
    database: process.env.db_name,
    password: process.env.db_password,
    port: 5432,
    max: 20, // Maximum number of clients in the pool 
    idleTimeoutMillis: 30000, // How long a client is allowed to remain idle before being closed 
});