//creating an interface 
export interface CreateDuty {
    id: string;
    name: string;
    created_at: string;
    completed: boolean;
}
export interface UpdateDuty {
    name: string;
    updated_at: string;
    completed: boolean;
}
export interface DeleteDuty {
    deleted_at: string;
}