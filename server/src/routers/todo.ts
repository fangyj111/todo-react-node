import express from "express";
import { TodoController } from '../controllers/todo.js'

//initiating the router
export const router = express.Router()
//get posts
router.get('/', TodoController.getTodoList);
router.get('/:id', TodoController.getTodo);
router.post('/', TodoController.createTodo);
router.put('/:id', TodoController.updateTodo);
router.delete('/:id', TodoController.deleteTodo);