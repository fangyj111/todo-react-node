import { pool } from '../db/pool.js';
class todoService {
    async getTodoList() {
        try {
            const sqlStr = 'SELECT * FROM duty WHERE deleted_at is null';
            const result = await pool.query(sqlStr);
            return result;
        }
        catch (error) {
            console.log(error);
        }
    }
    async getTodo(id) {
        try {
            const sqlStr = 'SELECT * FROM duty where id=$1 and deleted_at is null';
            const result = await pool.query(sqlStr, [id]);
            return result;
        }
        catch (error) {
            console.log(error);
        }
    }
    async createTodo(data) {
        try {
            const sqlStr = 'INSERT INTO duty (id, name, created_at,updated_at, deleted_at, completed) VALUES ($1, $2, $3, $4, $5, $6)  RETURNING id, name, created_at,updated_at, deleted_at, completed';
            const result = await pool.query(sqlStr, [data.id, data.name, data.created_at, null, null, data.completed]);
            // console.log(`result`, JSON.stringify(result));
            return result;
        }
        catch (error) {
            console.log(error);
        }
    }
    async updateTodo(id, data) {
        try {
            const sqlStr = 'UPDATE duty SET name=$1, updated_at=$2, completed=$3 where id=$4  RETURNING id, name, created_at,updated_at, deleted_at, completed';
            const result = await pool.query(sqlStr, [data.name, data.updated_at, data.completed, id]);
            console.log(`result`, JSON.stringify(result));
            return result;
        }
        catch (error) {
            console.log(error);
        }
    }
    async deleteTodo(id, data) {
        try {
            console.log(`data:`, JSON.stringify(data));
            const sqlStr = 'UPDATE duty SET deleted_at=$1 where id=$2 RETURNING id, name';
            const result = await pool.query(sqlStr, [data.deleted_at, id]);
            // console.log(`result`, JSON.stringify(result));
            return result;
        }
        catch (error) {
            console.log(error);
        }
    }
}
export const todoServices = new todoService();
